import App from '../App.vue'

const Index = () => import('@/pages/index.vue')
const MyDoctor = () => import('@/pages/myDoctor.vue')
const PersonCenter = () => import('@/pages/personCenter.vue')
const DoctorIndex = () => import('@/pages/doctor/index.vue')
const DoctorList = () => import('@/pages/doctor/list.vue')

// 当日挂号单
const RegistrationSlip = () => import('@/pages/registrationSlip/index.vue')
const RegistrationSlipDetail = () => import('@/pages/registrationSlip/detail.vue')

// 知情同意书
const Informed = () => import('@/pages/informed/index.vue')
const InformedDetail = () => import('@/pages/informed/detail.vue')

// 新生儿出生证
const Born = () => import('@/pages/born/index.vue')
const FirstSign = () => import('@/pages/born/firstSign.vue')
const MakeUpAppoint = () => import('@/pages/born/makeUpAppoint.vue')
const MakeUpAppointDetail = () => import('@/pages/born/makeUpAppointDetail.vue')
const Change = () => import('@/pages/born/change.vue')
const ChangeDetail = () => import('@/pages/born/changeDetail.vue')
const DetailList = () => import('@/pages/born/detailList.vue')
const Detail = () => import('@/pages/born/detail.vue')

// 新生儿户籍
const Domiciliary = () => import('@/pages/domiciliary/index.vue')
const DomiciliaryDetailList = () => import('@/pages/domiciliary/detailList.vue')
const DomiciliaryFirstSign = () => import('@/pages/domiciliary/firstSign.vue')
const DomiciliaryDetail = () => import('@/pages/domiciliary/detail.vue')

// 新生儿户籍
const Social = () => import('@/pages/social/index.vue')
const SocialDetailList = () => import('@/pages/social/detailList.vue')
const SocialFirstSign = () => import('@/pages/social/firstSign.vue')
const SocialDetail = () => import('@/pages/social/detail.vue')

// 就诊卡
const PatientCard = () => import('@/pages/patientCard/index.vue')
const AddCard = () => import('@/pages/patientCard/addCard.vue')
const BindCard = () => import('@/pages/patientCard/bindCard.vue')
const CardDetail = () => import('@/pages/patientCard/cardDetail.vue')

// 当日挂号
const Registered = () => import('@/pages/registered/index.vue')
const RegisteredDepartment = () => import('@/pages/registered/department.vue')
const RegisteredForm = () => import('@/pages/registered/form.vue')

// 支付页面
const Payment = () => import('@/pages/payment.vue')

// 预约挂号
const Reservation = () => import('@/pages/reservation/index.vue')
const ReservationDepartment = () => import('@/pages/reservation/department.vue')
const ReservationForm = () => import('@/pages/reservation/form.vue')
const ReservationFormNoCard = () => import('@/pages/reservation/formNoCard.vue')

// 住院缴费
const Outpatient = () => import('@/pages/outpatient/index.vue')

// 住院清单
const HospitalList = () => import('@/pages/hospitalList.vue')

// 预交住院押金
const Deposit = () => import('@/pages/deposit/index.vue')
const DepositList = () => import('@/pages/deposit/list.vue')
const DepositRecord = () => import('@/pages/deposit/record.vue')

const Blank = () => import('@/pages/blank.vue')

// 医院 科室
const HospitalIndex = () => import('@/pages/hospital/index.vue')
const DepartmentList = () => import('@/pages/hospital/departmentList.vue')
const DepartmentSearchDet = () => import('@/pages/hospital/departmentSearchDet.vue')
const DepartmentMain = () => import('@/pages/hospital/departmentMain.vue')
// 检验检查报告
const ReportIndex = () => import('@/pages/report/index.vue')
const ReportDet = () => import('@/pages/report/reportDet.vue')
const ReportDetInspect = () => import('@/pages/report/reportDetInspect.vue')
// 药品诊疗查询
const MedicalInquiry = () => import('@/pages/medicalInquiry/index.vue')
// 排班
const Arrange = () => import('@/pages/arrange/index.vue')
const ArrangeDet = () => import('@/pages/arrange/arrangeDet.vue')

// 婚姻登记
const Marriage = () => import('@/pages/marriage/index.vue')
const MarriageDetail = () => import('@/pages/marriage/detail.vue')
const MarriageFirstSign = () => import('@/pages/marriage/firstSign.vue')
const MarriageDetailList = () => import('@/pages/marriage/detailList.vue')
const MarriageDivorce = () => import('@/pages/marriage/divorce.vue')
const MarriageDivorceDetail = () => import('@/pages/marriage/divorceDetail.vue')

// 预防接种证办理预约
const Premunition = () => import('@/pages/premunition/index.vue')
const PremunitionDetail = () => import('@/pages/premunition/detail.vue')
const PremunitionFirstSign = () => import('@/pages/premunition/firstSign.vue')
const PremunitionDetailList = () => import('@/pages/premunition/detailList.vue')
const PremunitionIssue = () => import('@/pages/premunition/issue.vue')
const PremunitionIssueDetail = () => import('@/pages/premunition/issueDetail.vue')

const routes = [{
  path: '/',
  name: 'index',
  meta: {
    noIcon: true,
    title: '上饶县妇幼保健院'
  },
  component: Index
},
{
  path: '/blank',
  name: 'blank',
  meta: {
    title: '上饶县妇幼保健院'
  },
  component: Blank
},
{
  path: '/myDoctor',
  name: 'myDoctor',
  meta: {
    noIcon: true,
    title: '我的医生'
  },
  component: MyDoctor
},
{
  path: '/personCenter',
  name: 'personCenter',
  meta: {
    noIcon: true,
    title: '个人中心'
  },
  component: PersonCenter
},
{
  path: '/doctor',
  name: 'doctorIndex',
  meta: {
    noHeader: true,
    title: '医生详情'
  },
  component: DoctorIndex
},
{
  path: '/doctor/list',
  name: 'doctorList',
  meta: {
    noHeader: true,
    noIcon: true,
    title: '医生详情'
  },
  component: DoctorList
},
{
  path: '/registrationSlip',
  name: 'RegistrationSlip',
  meta: {
    title: '当日挂号单'
  },
  component: RegistrationSlip
},
{
  path: '/registrationSlip/detail',
  name: 'RegistrationSlipDetail',
  meta: {
    title: '挂号详情'
  },
  component: RegistrationSlipDetail
},
{
  path: '/patientCard',
  name: 'PatientCard',
  meta: {
    title: '我的就诊卡'
  },
  component: PatientCard
},
{
  path: '/patientCard/addCard',
  name: 'AddCard',
  meta: {
    title: '添加就诊卡'
  },
  component: AddCard
},
{
  path: '/patientCard/cardDetail',
  name: 'CardDetail',
  meta: {
    title: '电子健康卡'
  },
  component: CardDetail
},
{
  path: '/patientCard/bindCard',
  name: 'BindCard',
  meta: {
    title: '绑定就诊卡'
  },
  component: BindCard
},
{
  path: '/domiciliary',
  name: 'Domiciliary',
  meta: {
    title: '新生儿户籍办理预约'
  },
  component: Domiciliary
},
{
  path: '/domiciliary/detailList',
  name: 'DomiciliaryDetailList',
  meta: {
    title: '新生儿户籍办理预约查看'
  },
  component: DomiciliaryDetailList
},
{
  path: '/domiciliary/firstSign',
  name: 'DomiciliaryFirstSign',
  meta: {
    title: '新生儿户籍办理预约'
  },
  component: DomiciliaryFirstSign
},
{
  path: '/domiciliary/Detail',
  name: 'DomiciliaryDetail',
  meta: {
    title: '新生儿户籍办理预约详情'
  },
  component: DomiciliaryDetail
},
{
  path: '/social',
  name: 'Social',
  meta: {
    title: '新生儿社保办理预约'
  },
  component: Social
},
{
  path: '/social/detailList',
  name: 'SocialDetailList',
  meta: {
    title: '新生儿社保办理预约查看'
  },
  component: SocialDetailList
},
{
  path: '/social/firstSign',
  name: 'SocialFirstSign',
  meta: {
    title: '新生儿社保办理预约'
  },
  component: SocialFirstSign
},
{
  path: '/social/Detail',
  name: 'SocialDetail',
  meta: {
    title: '新生儿社保办理预约详情'
  },
  component: SocialDetail
},
{
  path: '/informed',
  name: 'Informed',
  meta: {
    title: '知情同意书'
  },
  component: Informed
},
{
  path: '/informed/detail',
  name: 'InformedDetail',
  meta: {
    title: '知情同意书详情'
  },
  component: InformedDetail
},
{
  path: '/born',
  name: 'Born',
  meta: {
    title: '新生儿出生证办理预约'
  },
  component: Born
},
{
  path: '/born/firstSign',
  name: 'FirstSign',
  meta: {
    title: '新生儿出生证办理预约'
  },
  component: FirstSign
},
{
  path: '/born/makeUpAppoint',
  name: 'MakeUpAppoint',
  meta: {
    title: '新生儿出生证补发预约'
  },
  component: MakeUpAppoint
},
{
  path: '/born/makeUpAppointDetail',
  name: 'MakeUpAppointDetail',
  meta: {
    title: '新生儿出生证补发预约详情'
  },
  component: MakeUpAppointDetail
},
{
  path: '/born/change',
  name: 'Change',
  meta: {
    title: '新生儿出生证变更预约'
  },
  component: Change
},
{
  path: '/born/changeDetail',
  name: 'ChangeDetail',
  meta: {
    title: '新生儿出生证变更预约详情'
  },
  component: ChangeDetail
},
{
  path: '/born/detailList',
  name: 'DetailList',
  meta: {
    title: '新生儿出生证办理预约查看'
  },
  component: DetailList
},
{
  path: '/born/detail',
  name: 'Detail',
  meta: {
    title: '新生儿出生证首签预约详情'
  },
  component: Detail
},
{
  path: '/registered',
  name: 'Registered',
  meta: {
    noIcon: true,
    title: '当日挂号'
  },
  component: Registered
},
{
  path: '/registered/department',
  name: 'RegisteredDepartment',
  meta: {
    title: '当日挂号'
  },
  component: RegisteredDepartment
},
{
  path: '/registered/form',
  name: 'RegisteredForm',
  meta: {
    title: '挂号办理'
  },
  component: RegisteredForm
},
{
  path: '/payment',
  name: 'Payment',
  meta: {
    title: '确认支付'
  },
  component: Payment
},
{
  path: '/reservation',
  name: 'Reservation',
  meta: {
    noIcon: true,
    title: '预约挂号'
  },
  component: Reservation
},
{
  path: '/reservation/department',
  name: 'ReservationDepartment',
  meta: {
    title: '预约挂号'
  },
  component: ReservationDepartment
},
{
  path: '/reservation/form',
  name: 'ReservationForm',
  meta: {
    title: '预约办理'
  },
  component: ReservationForm
},
{
  path: '/reservation/formNoCard',
  name: 'ReservationFormNoCard',
  meta: {
    title: '无卡预约'
  },
  component: ReservationFormNoCard
},
{
  path: '/outpatient',
  name: 'Outpatient',
  meta: {
    title: '门诊缴费'
  },
  component: Outpatient
},
{
  path: '/deposit',
  name: 'Deposit',
  meta: {
    noIcon: true,
    title: '预交住院押金'
  },
  component: Deposit
},
{
  path: '/deposit/record',
  name: 'DepositRecord',
  meta: {
    noIcon: true,
    title: '缴费记录'
  },
  component: DepositRecord
},
{
  path: '/deposit/list',
  name: 'DepositList',
  meta: {
    noIcon: true,
    title: '预交金列表'
  },
  component: DepositList
},
  // 医院 科室
{
  path: '/hospital',
  name: 'HospitalIndex',
  meta: {
    title: '医院主页'
  },
  component: HospitalIndex
},
{
  path: '/hospital/departmentList',
  name: 'DepartmentList',
  meta: {
    title: '科室列表'
  },
  component: DepartmentList
},
{
  path: '/hospital/departmentSearchDet',
  name: 'DepartmentSearchDet',
  meta: {
    title: '科室列表'
  },
  component: DepartmentSearchDet
},
{
  path: '/hospital/departmentMain',
  name: 'DepartmentMain',
  meta: {
    title: '科室主页'
  },
  component: DepartmentMain
},
  // 检验检查报告
{
  path: '/report',
  name: 'ReportIndex',
  meta: {
    title: '检验检查报告'
  },
  component: ReportIndex
},
{
  path: '/report/reportDet',
  name: 'ReportDet',
  meta: {
    title: '检验详情'
  },
  component: ReportDet
},
{
  path: '/report/reportDetInspect',
  name: 'ReportDetInspect',
  meta: {
    title: '检查详情'
  },
  component: ReportDetInspect
},
  // 药品诊疗查询
{
  path: '/medicalInquiry',
  name: 'MedicalInquiry',
  meta: {
    title: '药品诊疗查询'
  },
  component: MedicalInquiry
},
  // 排班
{
  path: '/arrange',
  name: 'arrange',
  meta: {
    title: '医生排班'
  },
  component: Arrange
},
{
  path: '/arrange/arrangeDet',
  name: 'ArrangeDet',
  meta: {
    title: '科室医生排班'
  },
  component: ArrangeDet
},
{
  path: '/hospitalList',
  name: 'HospitalList',
  meta: {
    title: '住院费清单'
  },
  component: HospitalList
},
{
  path: '/marriage',
  name: 'Marriage',
  meta: {
    title: '婚姻办理预约'
  },
  component: Marriage
},
{
  path: '/marriage/detail',
  name: 'MarriageDetail',
  meta: {
    title: '离婚预约办理详情'
  },
  component: MarriageDetail
},
{
  path: '/marriage/firstSign',
  name: 'MarriageFirstSign',
  meta: {
    title: '婚姻预约办理'
  },
  component: MarriageFirstSign
},
{
  path: '/marriage/detailList',
  name: 'MarriageDetailList',
  meta: {
    title: '婚姻办理预约查看'
  },
  component: MarriageDetailList
},
{
  path: '/marriage/divorce',
  name: 'MarriageDivorce',
  meta: {
    title: '离婚预约办理'
  },
  component: MarriageDivorce
},
{
  path: '/marriage/divorceDetail',
  name: 'MarriageDivorceDetail',
  meta: {
    title: '离婚预约办理详情'
  },
  component: MarriageDivorceDetail
},
{
  path: '/premunition',
  name: 'Premunition',
  meta: {
    title: '新生儿预防接种证办理'
  },
  component: Premunition
},
{
  path: '/premunition/detail',
  name: 'PremunitionDetail',
  meta: {
    title: '离婚预约办理详情'
  },
  component: PremunitionDetail
},
{
  path: '/premunition/firstSign',
  name: 'PremunitionFirstSign',
  meta: {
    title: '婴幼儿预防接种证办理预约'
  },
  component: PremunitionFirstSign
},
{
  path: '/premunition/detailList',
  name: 'PremunitionDetailList',
  meta: {
    title: '婴幼儿预防接种证办理详情'
  },
  component: PremunitionDetailList
},
{
  path: '/premunition/detailList',
  name: 'PremunitionDetailList',
  meta: {
    title: '新生儿预防接种证办理预约查看'
  },
  component: PremunitionDetailList
},
{
  path: '/premunition/issue',
  name: 'PremunitionIssue',
  meta: {
    title: '婴幼儿预防接种证补办预约'
  },
  component: PremunitionIssue
},
{
  path: '/premunition/issueDetail',
  name: 'PremunitionIssueDetail',
  meta: {
    title: '婴幼儿预防接种证补办详情'
  },
  component: PremunitionIssueDetail
}]

export default routes
